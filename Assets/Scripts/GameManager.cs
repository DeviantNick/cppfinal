﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour
{

    static GameManager _instance = null;

    public Canvas hudCanvas;

    public Text P1_Score;
    public Text P2_Score;

    public Text P1_BuffTimer;
    public Text P2_BuffTimer;

    public GameObject pickup1;
    public GameObject pickup2;
    public GameObject pickup3;

    public Transform rand1;
    public Transform rand2;
    public Transform rand3;
    public Transform rand4;

    public float initialDelay = 30;
    public float repeatTime= 30;


    //change
    public int p1Score = 0;

    public int p2Score = 0;

    public bool hitLast = false;

    private GameObject[] agents;
	// Use this for initialization
	void Start ()
    {
        //singleton code

        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

        InvokeRepeating("PickupTimer", initialDelay, repeatTime);
        P1_Score = GetComponent<Text>();
        P2_Score = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //quits game
	    if (Input.GetKey (KeyCode.Escape))
        {
            Application.Quit();
        }
        //would use to go from title screen to game. ignore for now.
        if (Input.GetKey (KeyCode.KeypadEnter) && SceneManager.GetActiveScene().name == "Main")
        {
            SceneManager.LoadScene("Pong");
        }
        //ends game if score of 10 is reached.
        //would ideally have an additional scene with winscreen text 
        //and the ability to restart from score 0 if i had the time
        if (p1Score >= 10 || p2Score >=10)
        {
            Application.Quit();
        }



        //for updating the UI score & bufftimers
        P1_Score.text = ": " + p1Score;
        P2_Score.text = ": " + p2Score;

        P1_BuffTimer.text = ": " + BallScript.instance.powerupTimer;
        P2_BuffTimer.text = ": " + BallScript.instance.powerupTimer;

        if (BallScript.instance.powerupTimer <=0)
        {
            P1_BuffTimer.text = "0";
            P2_BuffTimer.text = "0";
        }
    }


    public void PickupTimer()
    {
        GameObject deleteObj = GameObject.FindGameObjectWithTag("Powerup");

        Destroy(deleteObj);
        //spawns a projectile
        int rndNum1 = Random.Range(0, 3);
        int rndNum2 = Random.Range(0, 4);
        if (rndNum1 == 1)
        {
            if (rndNum2 == 1)
                Instantiate(pickup1, rand1.transform.position, Quaternion.identity);
            if (rndNum2 == 2)
                Instantiate(pickup1, rand2.transform.position, Quaternion.identity);
            if (rndNum2 == 3)
                Instantiate(pickup1, rand3.transform.position, Quaternion.identity);
            if (rndNum2 == 4)
                Instantiate(pickup1, rand4.transform.position, Quaternion.identity);
        }
        if (rndNum1 == 2)
        {
            if (rndNum2 == 1)
                Instantiate(pickup2, rand1.transform.position, Quaternion.identity);
            if (rndNum2 == 2)
                Instantiate(pickup2, rand2.transform.position, Quaternion.identity);
            if (rndNum2 == 3)
                Instantiate(pickup2, rand3.transform.position, Quaternion.identity);
            if (rndNum2 == 4)
                Instantiate(pickup2, rand4.transform.position, Quaternion.identity);
        }
        if (rndNum1 == 3)
        {
            if (rndNum2 == 1)
                Instantiate(pickup3, rand1.transform.position, Quaternion.identity);
            if (rndNum2 == 2)
                Instantiate(pickup3, rand2.transform.position, Quaternion.identity);
            if (rndNum2 == 3)
                Instantiate(pickup3, rand3.transform.position, Quaternion.identity);
            if (rndNum2 == 4)
                Instantiate(pickup3, rand4.transform.position, Quaternion.identity);
        }
    }

    public static GameManager instance
    {
        get { return _instance; } //is the same as  //get;
        set { _instance = value; }//is the same as  //set;
    }
}
