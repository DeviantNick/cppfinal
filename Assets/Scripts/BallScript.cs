﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BallScript : NetworkBehaviour
{

    static BallScript _instance = null;
    private const float speedIncrease = 0.5f;

    private Rigidbody rb;

    private Vector3 vel;

    public float powerupTimer = 1.0f;
    
    // Use this for initialization
    void Start ()
    {
        rb = this.GetComponent<Rigidbody>();
        //ResetBall();
        //singleton code
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }

    }

    // Update is called once per frame
    void Update ()
    {
        //transform.Translate(vel * Time.deltaTime, Space.World);

        if (Input.GetKey(KeyCode.P))
        {
            ResetBall();
        }

        if (powerupTimer > 1)
        {
            powerupTimer -= Time.deltaTime;
        }

        if (powerupTimer <= 0)
        {
            if (isLocalPlayer)
            {
                transform.localScale.Set(1, 1, 1);
            }
            else if (!isLocalPlayer)
            {
                transform.localScale.Set(1, 1, 1);
            }
        }
    }

    private void ResetBall()
    {
        transform.position = Vector3.zero;
        rb.velocity = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), Random.Range(-3, 3));
        //rb.angularVelocity = new Vector3();
        //rb.AddForce(1,1,1);
    }

    //not working the way i want it to, currently. I am not normalizing properly. Will work on later if i can.
    void OnCollisionEnter(Collision collision)
    {
        //on wall bounce
        if (collision.gameObject.tag == "Bounce")
        {
            vel = Vector3.Reflect(-vel, collision.transform.forward);
        }


        if (collision.gameObject.tag == "Player" && isLocalPlayer)
        {
            GameManager.instance.hitLast = true;

        }
        if (collision.gameObject.tag == "P1Goal")
        {
            GameManager.instance.p1Score++;
            ResetBall();
            //rb.velocity = new Vector3(collision.relativeVelocity.x, collision.relativeVelocity.y, collision.relativeVelocity.z );
            //rb.AddForce(Vector3.Reflect(rb.velocity,Vector3.right)* rb.velocity.magnitude, ForceMode.Impulse);
        }
        else if (collision.gameObject.tag == "P2Goal")
        {
            GameManager.instance.p2Score++;
            ResetBall();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Powerup")
        {   
            //ball speed increase
            if (other.name == "Pickup1")
            {
                if (isLocalPlayer && GameManager.instance.hitLast == true)
                {
                    rb.velocity = rb.velocity * 2;


                    //reset hitLast
                    GameManager.instance.hitLast = false;
                }
            }
            //paddle size boost
            else if (other.name == "Pickup2")
            {
                if (isLocalPlayer && GameManager.instance.hitLast == true)
                {
                    //increase my paddle size
                    transform.localScale += new Vector3(1, 1, 1);

                    powerupTimer = 15;

                    //reset hitLast
                    GameManager.instance.hitLast = false;
                }
            }
            //opponent paddle size decrease
            else if (other.name == "Pickup3")
            {
                
                if (!isLocalPlayer && GameManager.instance.hitLast == true)
                {
                    //decrease the opponent's paddle size
                    transform.localScale -= new Vector3(1, 1, 1);

                    powerupTimer = 15;

                    //reset hitLast
                    GameManager.instance.hitLast = false;
                }
            }
        }
    }

    public static BallScript instance
    {
        get { return _instance; } //is the same as  //get;
        set { _instance = value; }//is the same as  //set;
    }
}
