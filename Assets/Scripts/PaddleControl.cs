﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class PaddleControl : NetworkBehaviour {


    /*
    public KeyCode paddleUp = KeyCode.W;
    public KeyCode paddleDown = KeyCode.S;
    public KeyCode paddleLeft = KeyCode.A;
    public KeyCode paddleRight = KeyCode.D;
    */
    [SyncVar]
    private float horizontal;

    [SyncVar]
    private float vertical;



    public float paddleSpeed = 1.0f;

    private Rigidbody rb;


	// Use this for initialization
	void Start ()
    {
        rb = this.GetComponent<Rigidbody>();


    }



    public override void OnStartLocalPlayer()
    {
        if (isServer)
        {
            this.gameObject.GetComponentInChildren<Camera>().enabled = true;
            //this.transform.position = player1Spawn.transform.position;
            //transform.Rotate(180, 0, 0);
            //GetComponent<MeshRenderer>().material.color = Color.green;
        }

        if (isClient)
        {
            this.gameObject.GetComponentInChildren<Camera>().enabled = true;
            //this.transform.position = player2Spawn.transform.position;
            //transform.Rotate(180, 0, 0);
            //Camera.transform.rotation *= Quaternion.Euler(180, 0, 0);
        }

    }
	

	// Update is called once per frame
	void Update ()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var y = Input.GetAxis("Vertical") * Time.deltaTime * paddleSpeed;
        var z = Input.GetAxis("Horizontal") * Time.deltaTime * paddleSpeed;

        //transform.Rotate(0, 0, 0);
        transform.Translate(0, 0, z);
        transform.Translate(0, y, 0);

   

}

   
}


