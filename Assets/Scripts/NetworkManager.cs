﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkManager : NetworkBehaviour
{

    static NetworkManager _instance = null;
    //[SyncVar]
    public Transform player1Spawn;
    public Transform player2Spawn;

    public GameObject playerPrefab;

    // Use this for initialization
    void Start ()
    {
        //singleton code
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this);
        }



        player1Spawn = GameObject.FindWithTag("Player1Spawn").transform;
        player2Spawn = GameObject.FindWithTag("Player2Spawn").transform;


    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    //i was going to use this to manually spawn the player but learned a better way.
    public void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject player = (GameObject)Instantiate(playerPrefab, player1Spawn.transform.position, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    }

    public static NetworkManager instance
    {
        get { return _instance; } //is the same as  //get;
        set { _instance = value; }//is the same as  //set;
    }
}
